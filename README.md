Impulswahl zu Mehrfrequenzwahl Umsetzer
=======================================
Manche Voip Router verstehen keine Impulswahl von Wählscheibentelefonen.
Dies ist die Firmware zu einem auf dem MSP430 basierendem Konverter, der 
sich in das Telefon einbauen lässt und leitungsgespeist ist.

Die DTMF Töne werden über eine PWM erzeugt, das Vefahren wird in einer AN
von ATMEL für AVR MCUs erklärt und ist hier auf den MSP430 angepasst.

Das PWM Signal wird dann über ein Butterworth Tiefpass gefiltert über eine
Transistorstufe auf die Leitung moduliert.


