/***************************************************************************
 * Projekt:	IWV - MFV Umsetzer
 * 			Umsetzer zum Einbau in Wählscheibentelefone. Wertet die Wählimpulse
 * 			des NSI Kontakltes des Nummernschalters aus und generiert
 * 			entsprechende MFV Töne. Die Erzeugung der DTMF Töne ist von der
 * 			Atmel AN abgeleitet.
 * Autor: 	Jan-Ole Spahn
 * Version: 24.01.2016, Umstellung auf MSP430F2011, Hardware Rev.3
 * 			Kalibrierung des DCO mit LFXT1.
 * 			0.99, 9.10.2014, Verwendung des DCO statt Quarz, daher Töne über
 * 			Schrittweiten individuell abgestimmt. Funktionsumfang bisher nur
 * 			normale Wahl.
 ***************************************************************************/

#include <msp430.h> 
//#define DEBUG_TONE		// Statt normaler Funktion Dauerton

// Zustände für Nummernschalter Auswertung
#define NS_RUHE			0	// NS in Ruheszustand
#define NS_WARTEN		1	// NS aufgezogen, noch keine Impulse
#define NS_IMPULS		3	// Wahl läuft, NSI geöffnet (Impuls)
#define NS_PAUSE		4	// Wahl läuft, NSI geschlossen (Pause)
#define NS_WAHLENDE		5	// Wahl einer Ziffer beendet
#define NS_ABBRUCH		6	// Fehlerzustand (z.B. Timeout)

// Zeitkonstanten (F_MCLK = 8MHz, WDT ISR alle 4ms)
#define MFV_Zeit			50
#define NSI_PULS_MAX		20
#define NSI_PAUSE_MAX	30
#define NSA_HALTEZEIT	250

/************************** Sinus Tabelle**********************************
 * Einse Sinusperiode mit 128 Abtatsungen und 7 bit Auflösung
 *************************************************************************/
const unsigned char Sinus128 [128] = {
64,67,70,73,76,79,82,85,88,91,94,96,99,102,104,106,109,111,113,115,117,118,
120,121,123,124,125,126,126,127,127,127,127,127,127,127,126,126,125,124,
123,121,120,118,117,115,113,111,109,106,104,102,99,96,94,91,88,85,82,79,
76,73,70,67,64,60,57,54,51,48,45,42,39,36,33,31,28,25,23,21,18,16,14,12,
10,9,7,6,4,3,2,1,1,0,0,0,0,0,0,0,1,1,2,3,4,6,7,9,10,12,14,16,18,21,23,25,
28,31,33,36,39,42,45,48,51,54,57,60};

/**************************************************************************
 *  Tabelle der MFV Frequenzen
 *	Sinus Schrittweiten (*8) : x_SW = 8 * N_SinAbtast. * f_nf / f_pwm
 *	697Hz -> 22,8	1209Hz -> 39,6
 *	770Hz -> 25,2	1336Hz -> 43,8
 *	852Hz -> 27		1477Hz -> 48,4
 *	941Hz -> 30,8	1633Hz -> 53,5
 *************************************************************************/
const unsigned char MFV_Tabelle [12][2] = {
		{ 23, 40}, // 1: 697Hz & 1209Hz
		{ 23, 44}, // 2: 697Hz & 1336Hz
		{ 23, 48}, // 3: 697Hz & 1477Hz
		{ 25, 40}, // 4: 770Hz & 1209Hz
		{ 25, 44}, // 5: 770Hz & 1336Hz
		{ 25, 48}, // 6: 770Hz & 1477Hz
		{ 28, 40}, // 7: 852Hz & 1209Hz
		{ 28, 44}, // 8: 852Hz & 1336Hz
		{ 28, 48}, // 9: 852Hz & 1477Hz
		{ 31, 44}, // 0: 941Hz & 1336Hz
		{ 31, 40}, // *: 941Hz & 1209Hz
		{ 31, 48}  // #: 941Hz & 1477Hz
};

unsigned char x_SWtief = 0;		//	Schrittweite tiefe Freq.
unsigned char x_SWhoch = 0;     // Schrittweite hohe Freq.
unsigned int  x_LUTtiefExt = 0; // position freq. A in LUT (extended format)
unsigned int  x_LUThochExt = 0; // position freq. B in LUT (extended format)
unsigned int  x_LUTtief;        // position freq. A in LUT (actual position)
unsigned int  x_LUThoch;        // position freq. B in LUT (actual position)

/**************************************************************************
 *************************************************************************/
int main(void)
{
	WDTCTL = WDTPW | WDT_MDLY_32;	// WDT ISR alle 4ms da F-MCLK=8MHz
	IE1 |= WDTIE;
	
	P1DIR = BIT0 | BIT1 | BIT2 | BIT3 | BIT7;
	P1REN = BIT4 | BIT5 | BIT6;
	P1OUT = BIT4 | BIT5 | BIT6 ;
	P1SEL = BIT2;

	if (CALBC1_8MHZ==0xFF)					// If calibration constants erased
	{
		while(1);                               // do not load, trap CPU!!
	}
	DCOCTL = 0;                               // Select lowest DCOx and MODx settings
	BCSCTL1 = CALBC1_8MHZ;                    // Set range
	DCOCTL = CALDCO_8MHZ;                     // Set DCO step + modulation */

    /* Timer A PWM 31,25kHz */
    CCR0 = 255;                     // PWM Periode (32kHz)
    CCTL1 = OUTMOD_7 + CCIE;        // CCR1 reset/set
    TACTL = TASSEL_2 + MC_1;        // SMCLK, up mode
#ifdef DEBUG_TONE
    __delay_cycles(1000000);
    x_SWtief = 48;
    P1SEL |= BIT2;                  // P1.2 PWM Ausgang aktivieren
#endif

    _BIS_SR(LPM0_bits + GIE);       // Enter LPM0

}

#pragma vector=TIMERA1_VECTOR
__interrupt void TimerA(void)
{
	if (TAIV == 2){					// CCR1 interrupt?
	  // move Pointer about step width aheaed
	  x_LUTtiefExt += x_SWtief;
	  x_LUThochExt += x_SWhoch;
	  // normalize Temp-Pointer
	  x_LUTtief  =  (char)(((x_LUTtiefExt+4) >> 3)&(0x007F));
	  x_LUThoch  =  (char)(((x_LUThochExt+4) >> 3)&(0x007F));
	  // calculate PWM value: high frequency value + 3/4 low frequency value
#ifndef DEBUG_TONE
	  CCR1 = (Sinus128[x_LUTtief] + (Sinus128[x_LUThoch]-(Sinus128[x_LUThoch]>>2)));
#else
	  CCR1 = Sinus128[x_LUTtief];
#endif
	}
}

#pragma vector=WDT_VECTOR
__interrupt void WDT_Keys(void)
{
	static unsigned char NS_Status = NS_RUHE;
	static unsigned char NS_StatusNeu = NS_RUHE;
	static unsigned char NS_Restdauer = 0;
	static unsigned char MFV_Restdauer = 0;
	static unsigned char nsi = 0;
	static unsigned char NS_Ziffer = 0;

	nsi = (nsi<<1) | ((P1IN & BIT5)>>5) | 0xF0;	// NSI_Kontakt lesen

	switch (NS_Status){
	case NS_RUHE:
		if (nsi == 0xF1){
			NS_StatusNeu = NS_IMPULS;
			NS_Ziffer++;
			NS_Restdauer = NSI_PULS_MAX;
		}
		break;
	case NS_IMPULS:
		NS_Restdauer--;
		if (!NS_Restdauer){
			NS_StatusNeu = NS_ABBRUCH;
		}
		if (nsi == 0xFE){
			P1OUT &= ~BIT0;
			NS_StatusNeu = NS_PAUSE;
			NS_Restdauer = NSI_PAUSE_MAX;
		}
		break;
	case NS_PAUSE:
		NS_Restdauer--;
		if (!NS_Restdauer){
			NS_StatusNeu = NS_WAHLENDE;
			NS_Status = NS_StatusNeu;
		}
		if (nsi == 0xF1){
			NS_StatusNeu = NS_IMPULS;
			NS_Ziffer++;
			NS_Restdauer = NSI_PULS_MAX;
		}
		break;
	case NS_WAHLENDE:
		MFV_Restdauer = MFV_Zeit;
		P1SEL |= BIT2;                  			// P1.2  PWM Ausgang
        x_SWtief = MFV_Tabelle[NS_Ziffer-1][1]; // Schrittweite für niedrigen ...
        x_SWhoch = MFV_Tabelle[NS_Ziffer-1][0]; // ... und hohen Ton aus Tabelle holen
        NS_Ziffer = 0;
        NS_Restdauer = 0;
        NS_StatusNeu = NS_RUHE;
        NS_Status = NS_StatusNeu;
		break;
	case NS_ABBRUCH:
		NS_Ziffer = 0;
		NS_StatusNeu = NS_RUHE;
		break;
	}
	NS_Status = NS_StatusNeu;

#ifndef DEBUG_TONE
	if (MFV_Restdauer){
		MFV_Restdauer--;
	} else {
		P1SEL &= ~BIT2;    // P1.2 normaler output (und damit low)
	}
#endif

}
